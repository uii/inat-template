#include <iostream>

#include "common/networking.h"
#include "common/NetworkError.h"

// Печатает сообщения о любых ошибках.
void handle_errors();

int main()
try
{
    // Пример использования учебной библиотеки.
    SOCKET channel = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (NET_is_invalid(channel)) {
        RAISE_NETWORK_ERROR();
    }
    close(channel);
}
catch (...)
{
    handle_errors();
}


void handle_errors()
try
{
    throw;
}
catch (const NetworkError& error)
{
    std::cerr << "ERROR at " << error.getFile() << ":" << error.getLine()
              << " in " << error.getPlace() << ":\n\t" << error.getMessage()
              << std::endl;
    std::exit(EXIT_FAILURE);
}
catch (const std::exception& exception) {
    std::cerr << "ERROR:\n\t" << exception.what() << std::endl;
    std::exit(EXIT_FAILURE);
}
catch (...)
{
    std::cerr << "Unknown ERROR!" << std::endl;
    std::exit(EXIT_FAILURE);
}
