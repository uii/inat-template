#
# Подключение вспомогательных библиотек и настройка компоновщика.
# Редактировать не нужно.
#
cmake_minimum_required(VERSION 2.6)
if (${WIN32})
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static")
    macro(add_executable _name)
        _add_executable(${ARGV})
        if (TARGET ${_name})
            target_link_libraries(${_name} ws2_32)
        endif()
    endmacro()
endif(${WIN32})
add_subdirectory(common)

#
# Настройка конкретного проекта, отредактировать по необходимости.
# Проект будет называться так, как указано в project().
# Если файлов кода несколько, их пожно перечислить в add_executable() через пробел.
#
project(lab)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra -Werror -pedantic-errors")
add_executable(${PROJECT_NAME} main.cpp)
target_link_libraries(${PROJECT_NAME} Networking)

# Для лабораторных работ 2 и 3 можно сделать так:
#
#   project(lab2)
#   add_executable(client client.cpp)
#   target_link_libraries(client Networking)
#   add_executable(server server.cpp)
#   target_link_libraries(server Networking)
